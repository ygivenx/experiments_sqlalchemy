import pytest
from mock import patch
from main import models
from main.models import Base


@pytest.fixture
def set_env(monkeypatch):
    """

    """
    monkeypatch.setenv("CONNECTION_STRING", "sqlite:///awesomedb")
    yield set_env


@pytest.fixture
def mock_connection(set_env):
    """

    """
    from main.dbconn import SQLAlchemyConnection
    yield SQLAlchemyConnection()


@pytest.fixture
def mock_request():
    """
    a common request mocker

    :return: mock request
    """
    with patch('requests.get') as mock_request:
        resp_text = {'userId': 1, 'id': 1, 'title': 'test', 'body': 'test\nmy\nbody'}

        mock_request.return_value = resp_text

    yield mock_request


@pytest.fixture(autouse=True)
def database(mock_connection):
    """
    Database for test

    :return:
    """
    with mock_connection as conn:
        Base.metadata.drop_all(conn.engine)
        Base.metadata.create_all(conn.engine)

        conn.session.add(models.Users(**{"id": 1, "name": "Rohan Singh"}))
        conn.session.add(models.Address(**{"id": 1, "email": "awesome@great.com", "user_id": 1}))

        print("Created database..")

    yield database

    Base.metadata.drop_all(conn.engine)
