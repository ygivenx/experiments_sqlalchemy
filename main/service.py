from main import dbconn
from main import models


@dbconn.db_connection
def list_user_ids(conn, user_id):
    """
    Get a list of users in the DB table

    :return:
    """
    data = conn.session.query(models.Users.name).filter(models.Users.id == user_id).one()

    return data[0]

