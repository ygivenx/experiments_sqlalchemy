"""
Module to load data into postgres

"""
import hashlib
import json
from main import dbconn
from main.models import Base


def current_hash():
    """
    Get the current hashes

    """
    with open("../data/hashes.json") as file_pointer:
        hashes = json.load(file_pointer)
        print(hashes)
        return hashes


def find_hash(data):
    """
    Find hash of the given data content

    """
    return hashlib.sha256(data).hexdigest()


def load_files():
    """
    Load data to SQL

    """
    hashes = current_hash()
    load_order = ["users", "address", "stackoverflow"]  # order of tables

    for tn in load_order:
        file_name = f'../data/{tn}.txt'
        if file_name.endswith("txt"):
            with open(file_name) as f:
                read_data = f.read()
            hash = find_hash(read_data.encode())
            if tn in hashes.keys() and hashes[tn] == hash:
                print("same data.. no reloading")
            else:
                hashes[tn] = hash
                print("Loading: ", file_name)
                with dbconn.SQLAlchemyConnection() as conn:

                    # This can be normal sql alchemy loads
                    # I like if it can be done using POSTGRES specific commands to be faster moving forward from MVP

                    with open(file_name, "r") as _io_buffer:
                        copy_from = """
                        COPY "%s"
                        FROM STDIN
                        WITH (
                        FORMAT CSV,
                        DELIMITER ',',
                        HEADER
                        );
                        """
                        connection = conn.engine.raw_connection()
                        cursor = connection.cursor()
                        cursor.copy_expert(copy_from % tn, file=_io_buffer)
                        connection.commit()

    with open("../data/hashes.json", "w") as file_pointer:
        json.dump(hashes, file_pointer)


if __name__ == "__main__":
    with dbconn.SQLAlchemyConnection() as conn:
        Base.metadata.drop_all(conn.engine)
        Base.metadata.create_all(conn.engine)
    load_files()
