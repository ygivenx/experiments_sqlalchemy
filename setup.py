from distutils.core import setup

setup(
    name='sqlalchemy',
    version='0.1',
    packages=['main', 'test'],
    url='',
    license='MIT',
    author='rohan.singh@ibm.com',
    author_email='singhrohan@outlook.com',
    description=''
)
